﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour {

    public bool permitidoDiminuirX = true, permitidoDiminuirZ = true, permitidoAumentarX = true, permitidoAumentarZ = true;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        float movimentoX = Input.GetAxis("Horizontal") * 0.05f;
        float movimentoZ = Input.GetAxis("Vertical") * 0.05f;
        if(!permitidoDiminuirX && movimentoX < 0)
        {
            movimentoX = 0;
        }
        if (!permitidoAumentarX && movimentoX > 0)
        {
            movimentoX = 0;
        }
        if (!permitidoDiminuirZ && movimentoZ < 0)
        {
            movimentoZ = 0;
        }
        if (!permitidoAumentarZ && movimentoZ > 0)
        {
            movimentoZ = 0;
        }
        this.transform.position = new Vector3(movimentoX + this.transform.position.x, this.transform.position.y, movimentoZ + this.transform.position.z);
	}
}
