﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileController : MonoBehaviour {

    public GameObject projetilPrefab, visao;
    public float tempoCriacaoProjetil = 2f, tempoDesteUltimoTiro = 0f;
    public bool projetilDisparado = false;

    // Use this for initialization
    void Start () {
        criarProjetil();
	}
	
	// Update is called once per frame
	void Update () {
        if (projetilDisparado)
        {
            tempoDesteUltimoTiro += Time.deltaTime;
            if (tempoDesteUltimoTiro >= tempoCriacaoProjetil)
            {
                criarProjetil();
                tempoDesteUltimoTiro = 0;

                if (!this.gameObject.CompareTag("Nave Inimiga"))
                   projetilDisparado = false;
            }
        }
    }

    void criarProjetil()
    {
        GameObject projetilInstanciado = Instantiate(projetilPrefab);
        projetilInstanciado.transform.parent = this.transform;
        projetilInstanciado.transform.localScale = new Vector3(1f, 1f, 1f);
        projetilInstanciado.AddComponent<ProjectileMovementController>();
        ProjectileMovementController projectileController = projetilInstanciado.GetComponent<ProjectileMovementController>();
        projectileController.nave = this.gameObject;
        if (this.gameObject.CompareTag("Nave Jogador"))
        {
            projectileController.ajustePosicaoX = 0.3f;
            projectileController.ajustePosicaoY = -0.2f;
            projectileController.ajustePosicaoZ = 1.25f;
        }
        if (this.gameObject.CompareTag("Nave Inimiga"))
        {
            projectileController.ajustePosicaoX = 0f;
            projectileController.ajustePosicaoY = -0.2f;
            projectileController.ajustePosicaoZ = -0.90f;
            projectileController.isFired = true;
            projectileController.transform.parent = null;
            projetilDisparado = true;
        }
        projetilInstanciado.AddComponent<ProjectileMovementLimitController>();
    }
}
