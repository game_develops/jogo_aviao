﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

	public GameObject naveHabilitada, cuboNaveHabilitada;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void acionarNovoInimigo(GameObject novaNave, GameObject cuboNovaNave){
		naveHabilitada.SetActive(false);
		if(novaNave != null){
			this.naveHabilitada = novaNave;
			this.cuboNaveHabilitada = cuboNovaNave;
			naveHabilitada.SetActive(true);
		}
	}
}
