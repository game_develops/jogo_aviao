﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorController : MonoBehaviour {

	public GameObject meteoro, limiteXMinimo, limiteXMaximo;
	public float tempoCriacao = 10f;

	// Use this for initialization
	void Start () {
		atualizarTempoCriacao();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void atualizarTempoCriacao(){
		CancelInvoke("criarMeteoro");
		InvokeRepeating("criarMeteoro", 0f, tempoCriacao);
	}

	void criarMeteoro(){
        GameObject meteoroInstanciado = Instantiate(meteoro);
        meteoroInstanciado.transform.position = new Vector3(Random.Range(limiteXMinimo.transform.position.x, limiteXMaximo.transform.position.x), this.transform.position.y, this.transform.position.z);
	}
}
