﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileMovementLimitController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("LimiteZMaximo"))
        {
            Destroy(this.gameObject);
        }
        if (other.gameObject.CompareTag("LimiteZMinimo"))
        {
            Destroy(this.gameObject);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Inimigo") && !this.gameObject.CompareTag("Projetil Inimigo"))
        {
            Destroy(this.gameObject);
        }
        if (other.gameObject.CompareTag("Jogador") && !this.gameObject.CompareTag("Projetil"))
        {
            Destroy(this.gameObject);
        }
    }

}
