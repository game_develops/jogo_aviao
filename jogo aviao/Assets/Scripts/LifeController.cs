﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeController : MonoBehaviour {

    int vida = 100;
    public GameObject contadorVida;
    public GameObject proximaNave, cuboProximaNave;
    public EnemyController enemyController;
    public MeteorController meteorController;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Projetil") && !this.gameObject.CompareTag("Jogador"))
        {
            reduzirVida();
        }
        if (other.gameObject.CompareTag("Projetil Inimigo") && !this.gameObject.CompareTag("Inimigo"))
        {
            reduzirVida();
        }
    }

    private void reduzirVida()
    {
        vida -= 10;
        contadorVida.GetComponent<TextMesh>().text = vida.ToString();
        if(vida <= 0){
            enemyController.acionarNovoInimigo(proximaNave, cuboProximaNave);
            meteorController.tempoCriacao -= 4.5f;
            meteorController.atualizarTempoCriacao();
        }
    }
}
