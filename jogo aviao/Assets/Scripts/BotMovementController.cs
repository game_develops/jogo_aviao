﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotMovementController : MonoBehaviour {

	public TipoMovimento tipoMovimento = TipoMovimento.estatico;
	public GameObject player, limiteXMaximo, limiteXMinimo;
	float valorMovimento = 2.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		switch(tipoMovimento){
			case TipoMovimento.estatico:
				movimentoEstatico();
				break;
			case TipoMovimento.dinamico:
				movimentoDinamico();
				break;
			case TipoMovimento.perseguir:
				movimentoPerseguir();
				break;
		}
	}

	void movimentoEstatico () {

	}

	void movimentoDinamico () {
		float adicaoMovimento = Time.deltaTime * valorMovimento;
        this.transform.position = new Vector3(this.transform.position.x + adicaoMovimento, this.transform.position.y, this.transform.position.z);
        if(this.transform.position.x >= limiteXMaximo.transform.position.x){
        	valorMovimento *= -1;
        }
        if(this.transform.position.x <= limiteXMinimo.transform.position.x){
        	valorMovimento *= -1;
        }
	}

	void movimentoPerseguir () {
        this.transform.position = new Vector3(player.transform.position.x, this.transform.position.y, this.transform.position.z);
	}
}

public enum TipoMovimento{
	estatico,dinamico,perseguir
}
