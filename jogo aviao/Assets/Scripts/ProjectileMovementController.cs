﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileMovementController : MonoBehaviour {

    public bool isFired = false;
    public float speed = 0.1f;
    public GameObject nave;
    public float ajustePosicaoX, ajustePosicaoY, ajustePosicaoZ;

	// Use this for initialization
	void Start () {
        this.transform.position = new Vector3(nave.transform.position.x + ajustePosicaoX, nave.transform.position.y + ajustePosicaoY, nave.transform.position.z + ajustePosicaoZ);
        if (this.gameObject.CompareTag("Projetil Inimigo"))
        {
            speed *= -1;
        }
    }

    // Update is called once per frame
    void Update () {
        if(isFired){
            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, speed + this.transform.position.z);
        }
        if(Input.GetKeyDown(KeyCode.Space) && !this.gameObject.CompareTag("Projetil Inimigo")){
            isFired = true;
            this.transform.parent = null;
            ProjectileController projectileController = nave.GetComponent<ProjectileController>();
            projectileController.projetilDisparado = true;
        }
	}

}
