﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementLimitController : MonoBehaviour
{

    public MovementController movementController;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("LimiteXMinimo"))
        {
            movementController.permitidoDiminuirX = false;
        }
        if (other.gameObject.CompareTag("LimiteXMaximo"))
        {
            movementController.permitidoAumentarX = false;
        }
        if (other.gameObject.CompareTag("LimiteZMinimo"))
        {
            movementController.permitidoDiminuirZ = false;
        }
        if (other.gameObject.CompareTag("LimiteZMaximo"))
        {
            movementController.permitidoAumentarZ = false;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("LimiteXMinimo"))
        {
            movementController.permitidoDiminuirX = true;
        }
        if (other.gameObject.CompareTag("LimiteXMaximo"))
        {
            movementController.permitidoAumentarX = true;
        }
        if (other.gameObject.CompareTag("LimiteZMinimo"))
        {
            movementController.permitidoDiminuirZ = true;
        }
        if (other.gameObject.CompareTag("LimiteZMaximo"))
        {
            movementController.permitidoAumentarZ = true;
        }
    }
}